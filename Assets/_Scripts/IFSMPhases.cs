using homelleon.Battler.Implementation.FSM.Phases;
using System;

namespace homelleon.Battler
{
	public interface IFSMPhases : IFinitStateMachine<BasePhaseState>
	{
		void End();
	}
}
