using System;
using System.Collections.Generic;
using UnityEngine;

namespace homelleon.Battler.Implementation.FSM
{
	public abstract class BaseFSM<T> where T : BaseState
	{
		private Dictionary<Type, T> _states = new Dictionary<Type, T>();
		protected BaseState _currentState;
		public void Switch<V>() where V : T => Switch(typeof(V));

		public void Switch(Type type)
		{
			Debug.Log("Switching to: " + type);
			_currentState?.Exit();
			var prevState = _currentState;
			_currentState = _states[type];
			_currentState?.Enter(prevState?.GetType());
		}

		public void Update() => _currentState?.Update();

		public void Add(T state) => _states.Add(state.GetType(), state);
	}
}
