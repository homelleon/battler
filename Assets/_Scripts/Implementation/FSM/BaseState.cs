using System;

namespace homelleon.Battler.Implementation.FSM
{
	public abstract class BaseState
	{
		protected Type _previousStateType;
		public virtual void Enter(Type previousStateType) 
		{
			_previousStateType = previousStateType;
		}
		public void Enter<T>() where T : BaseState => Enter(typeof(T));
		public virtual void Exit() { }
		public virtual void Update() { }
	}
}
