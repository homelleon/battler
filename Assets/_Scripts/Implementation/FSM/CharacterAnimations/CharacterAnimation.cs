namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public enum CharacterAnimation
	{
		None,
		Idle,
		Fight,
		Damaged,
		Death,
		Buff
	}
}
