using homelleon.Battler.Implementation.View;
using System;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public abstract class CharacterBaseState : BaseState
	{
		protected CharacterAnimator _fsm;
		protected CharacterMono _mono;
		protected abstract CharacterAnimation TargetAnimation { get; }
		public CharacterBaseState(CharacterAnimator fsm, CharacterMono mono)
		{ 
			_fsm = fsm;
			_mono = mono;
		}

		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_mono.Animator.speed = 1f;
			_mono.Animator.Play(TargetAnimation.ToString());
		}

		public override void Exit()
		{
			base.Exit();
			_mono.Animator.speed = 0f;
			_mono.Animator.Play(_mono.Animator.GetCurrentAnimatorStateInfo(0).fullPathHash, -1, 0f);
		}

		public virtual void Next() => _fsm.Switch<CharacterIdleState>();
	}
}
