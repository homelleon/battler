using homelleon.Battler.Implementation.View;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public class CharacterIdleState : CharacterBaseState
	{
		public CharacterIdleState(CharacterAnimator fsm, CharacterMono mono) : base(fsm, mono)
		{
		}

		protected override CharacterAnimation TargetAnimation => CharacterAnimation.Idle;
	}
}
