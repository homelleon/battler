using homelleon.Battler.Implementation.View;
using System;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public class CharacterBuffState : CharacterBaseState
	{
		private const string SOUND_NAME = "Buff";
		public CharacterBuffState(CharacterAnimator fsm, CharacterMono mono) : base(fsm, mono)
		{
		}

		protected override CharacterAnimation TargetAnimation => CharacterAnimation.Buff;
		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_mono.PlaySound(SOUND_NAME);
			_mono.PlayBuffVFX();
		}
	}
}
