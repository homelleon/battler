using homelleon.Battler.Implementation.View;
using System;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public class CharacterFightState : CharacterBaseState
	{
		private const string SOUND_NAME = "AxeAttack";
		public CharacterFightState(CharacterAnimator fsm, CharacterMono mono) : base(fsm, mono)
		{
		}

		protected override CharacterAnimation TargetAnimation => CharacterAnimation.Fight;
		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_mono.PlaySound(SOUND_NAME);
		}
	}
}
