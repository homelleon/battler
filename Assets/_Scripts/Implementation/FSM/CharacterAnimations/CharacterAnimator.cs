using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public class CharacterAnimator : BaseFSM<CharacterBaseState>
	{
		public CharacterAnimator(CharacterMono mono, CharacterProperty hp)
		{
			Add(new CharacterIdleState(this, mono));
			Add(new CharacterFightState(this, mono));
			Add(new CharacterDamagedState(this, mono, hp));
			Add(new CharacterDeathState(this, mono));
			Add(new CharacterBuffState(this, mono));
			Switch<CharacterIdleState>();
			mono.OnNext = Next;
		}

		public void Next() => ((CharacterBaseState) _currentState).Next();
	}
}
