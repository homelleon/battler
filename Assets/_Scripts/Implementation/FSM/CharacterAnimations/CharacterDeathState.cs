using homelleon.Battler.Implementation.View;
using System;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public class CharacterDeathState : CharacterBaseState
	{
		private const string SOUND_NAME = "Death";
		public CharacterDeathState(CharacterAnimator fsm, CharacterMono mono) : base(fsm, mono)
		{
		}

		protected override CharacterAnimation TargetAnimation => CharacterAnimation.Death;

		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_mono.PlaySound(SOUND_NAME);
		}
	}
}
