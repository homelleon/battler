using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View;
using System;

namespace homelleon.Battler.Implementation.FSM.CharacterAnimations
{
	public class CharacterDamagedState : CharacterBaseState
	{
		private CharacterProperty _hp;
		public CharacterDamagedState(CharacterAnimator fsm, CharacterMono mono, CharacterProperty hp) : base(fsm, mono)
		{
			_hp = hp;
		}

		protected override CharacterAnimation TargetAnimation => CharacterAnimation.Damaged;
		public override void Enter(Type previousStateType)
		{
			_mono.PlayBloodVFX();
			if (_hp.ModifiedContainer.Value <= _hp.Min)
				_fsm.Switch<CharacterDeathState>();
			else
				base.Enter(previousStateType);
		}
	}
}
