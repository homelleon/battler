using homelleon.Battler.Implementation.View.UI;
using System;
using System.Collections.Generic;

namespace homelleon.Battler.Implementation.FSM.Turns
{
	public class GameOverTurnState : BaseTurnState
	{
		private CanvasUI _ui;
		private List<Character> _characters;
		private IBattleRoundCounter _counter;
		public GameOverTurnState(IFSMTurns fsm, CanvasUI ui, List<Character> characters, IBattleRoundCounter counter) : base(fsm)
		{
			_ui = ui;
			_characters = characters;
			_counter = counter;
		}

		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_counter.Reset();
			_ui.RestartUI.Button.onClick.AddListener(OnRestart);
			_ui.RestartUI.Show();
		}

		private void OnRestart()
		{
			_fsm.Switch<LeftPlayerTurnState>();
			_characters.ForEach(character => character.Revive());
		}

		public override void Exit()
		{
			base.Exit();
			_ui.RestartUI.Button.onClick.RemoveListener(OnRestart);
			_ui.RestartUI.Hide();
		}
	}
}
