using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace homelleon.Battler.Implementation.FSM.Turns
{
	public abstract class PayerTurnBaseState<T> : BaseTurnState where T : BaseTurnState
	{
		private const int MAX_BUFF_COUNT = 2;

		private IBattleRoundCounter _counter;
		private List<CharacterProperty> _hitPoints;
		private CharacterUI _characterUI;
		private Character _character;
		private IBuffGenerator _buffGenerator;

		public PayerTurnBaseState(IFSMTurns fsm, CharacterUI characterUI, IBattleRoundCounter counter, 
			Character character, List<CharacterProperty> hitPoints, IBuffGenerator buffGenerator) : base(fsm)
		{
			_characterUI = characterUI;
			_counter = counter;
			_hitPoints = hitPoints;
			_character = character;
			_buffGenerator = buffGenerator;
		}

		private void OnBuff()
		{
			_characterUI.Control.BuffButton.Button.interactable = false;
			var characterBuffs = _character.GetBuffList();

			var excludeList = characterBuffs.Select(buff => buff.Type).ToList();

			var buff = _buffGenerator.Generate(excludeList);
			excludeList.Add(buff.Type);
			_character.AddBuff(buff);
			_characterUI.Buffs.Add(buff);
		}

		private void OnAttack()
		{
			_characterUI.Control.AttackButton.Button.interactable = false;
			_characterUI.Control.BuffButton.Button.interactable = false;
			if (!_hitPoints.Any(hp => hp.ModifiedContainer.Value <= hp.Min))
				_fsm.Switch<T>();
		}

		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_counter.Increment();
			_characterUI.Control.AttackButton.Button.interactable = true;

			if (_character.GetBuffList().Count < MAX_BUFF_COUNT)
				_characterUI.Control.BuffButton.Button.interactable = true;

			_characterUI.Control.AttackButton.Button.onClick.AddListener(OnAttack);
			_characterUI.Control.BuffButton.Button.onClick.AddListener(OnBuff);
		}

		public override void Exit()
		{
			base.Exit();
			_characterUI.Control.AttackButton.Button.interactable = false;
			_characterUI.Control.BuffButton.Button.interactable = false;
			_characterUI.Control.AttackButton.Button.onClick.RemoveListener(OnAttack);
			_characterUI.Control.BuffButton.Button.onClick.RemoveListener(OnBuff);
			_character.GetBuffList().ForEach(buff => buff.Consume());
		}
	}
}
