using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View.UI;
using System.Collections.Generic;

namespace homelleon.Battler.Implementation.FSM.Turns
{
	public class LeftPlayerTurnState : PayerTurnBaseState<RightPlayerTurnState>
	{
		public LeftPlayerTurnState(IFSMTurns fsm, CharacterUI characterUI, IBattleRoundCounter counter, Character character, List<CharacterProperty> hitPoints, IBuffGenerator buffGenerator) : base(fsm, characterUI, counter, character, hitPoints, buffGenerator)
		{
		}
	}
}
