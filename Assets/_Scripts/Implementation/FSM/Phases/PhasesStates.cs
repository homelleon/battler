using homelleon.Battler.Implementation.FSM.Turns;
using System;

namespace homelleon.Battler.Implementation.FSM.Phases
{
	public class PhasesStates : BaseTurnState, IFSMPhases
	{
		private FSMPhases _phases;
		public PhasesStates(FSMTurns turnsFSM) : base(turnsFSM)
		{
			_phases = new FSMPhases();
		}

		public void End()
		{
			_phases.End();
			_fsm.Switch(_previousStateType);
		}

		public void Switch(Type type) => _phases.Switch(type);
		public void Switch<V>() where V : BasePhaseState => Switch(typeof(V));

		public override void Enter(Type previousStateType)
		{
			base.Enter(previousStateType);
			_phases.Switch<IdlePhaseState>();
		}

		public override void Exit() 
		{ 
			base.Exit();
			_phases.End();
		}

		public void Add(BasePhaseState state) => _phases.Add(state);
	}
}
