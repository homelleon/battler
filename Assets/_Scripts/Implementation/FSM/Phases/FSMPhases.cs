using System;

namespace homelleon.Battler.Implementation.FSM.Phases
{
	public class FSMPhases : BaseFSM<BasePhaseState>
	{
		public void End()
		{
			_currentState?.Exit();
			_currentState = null;
		}
	}
}
