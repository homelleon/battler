namespace homelleon.Battler.Implementation.FSM.Phases
{
	public abstract class BasePhaseState : BaseState
	{
		protected IFSMPhases _fsm;
		public BasePhaseState(IFSMPhases fsm)
		{
			_fsm = fsm;
		}
	}
}
