using System.Text;
using UniRx;

namespace homelleon.Battler.Implementation
{
	public class BattleRoundCounter : IBattleRoundCounter
    {
        public ReactiveProperty<string> Text { get; private set; } = new ReactiveProperty<string>();
        private const string LEFT_PLAYER_NAME = "1";
        private const string RIGHT_PLAYER_NAME = "2";
        private const string DEFAULT_PLAYER_NAME = "Player";
        private const string ROUND_NAME = "Round";
        private const string TURN_NAME = "Turn";
        private const int PLAYER_COUNT = 2;
        private const string GAME_OVER_TEXT = "Game Over";
        private const string SPACE_TEXT = " ";
        private int _roundCount = 0;
        private int _turnCount = 0;

        public void Reset()
        {
            _turnCount = 0;
            _roundCount = 0;
            Text.Value = GAME_OVER_TEXT;
        }

        public void Increment()
        {
            _turnCount++;
            _roundCount = (_turnCount + 1) / PLAYER_COUNT;
            Text.Value = new StringBuilder()
                .Append(ROUND_NAME)
                .Append(SPACE_TEXT)
                .Append(_roundCount)
                .Append(SPACE_TEXT)
                .Append(DEFAULT_PLAYER_NAME)
                .Append(_turnCount % PLAYER_COUNT == 0 ? RIGHT_PLAYER_NAME : LEFT_PLAYER_NAME)
                .Append(SPACE_TEXT)
                .Append(TURN_NAME)
                .ToString();
        }
    }
}
