﻿using homelleon.Battler.Implementation.PropertyData;
using System;

namespace homelleon.Battler.Implementation.Buffs
{
	[Serializable]
	public class Modificator
	{
		public PropertyType TargetProperty;
		public ModificatorTarget Target;
		public ModificatorType Type;
		public ModificatorDuration Duration;
		public BuffExecuteType ExecuteType;
		public float Value;
		public Action OnRemove;

		public void Apply(ref float value)
		{
			switch (Type)
			{
				case ModificatorType.Muliply:
					value *= Value;
					break;

				case ModificatorType.Divide:
					value /= Value;
					break;
				case ModificatorType.Plus:
					value += Value;
					break;
				case ModificatorType.Minus:
					value -= Value;
					break;
			}
		}

		public void Remove(ref float value)
		{
			switch (Type)
			{
				case ModificatorType.Muliply:
					value /= Value;
					break;

				case ModificatorType.Divide:
					value *= Value;
					break;
				case ModificatorType.Plus:
					value -= Value;
					break;
				case ModificatorType.Minus:
					value += Value;
					break;
			}
		}
	}
}
