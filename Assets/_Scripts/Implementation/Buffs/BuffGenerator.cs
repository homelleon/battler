using homelleon.Battler.Configs;
using ModestTree;
using System;
using System.Collections.Generic;
using System.Linq;

namespace homelleon.Battler.Implementation.Buffs
{
    public class BuffGenerator : IBuffGenerator
    {
        private List<BuffRawData> _buffs = new List<BuffRawData>();
        private Random _random = new Random();
        public BuffGenerator(BuffsConfig buffsConfig) 
        {
            foreach (var item in buffsConfig.Array)
                _buffs.Add(item);
        }
        public CharacterBuff Generate(List<BuffType> excludeBuffs = null)
        {
            var buffs = (excludeBuffs == null || excludeBuffs.IsEmpty()) ?
                _buffs.ToArray() :
                _buffs
                    .Where(buff => !excludeBuffs.Contains(buff.Type))
                    .ToArray();

            var index = _random.Next(0, buffs.Length);
            return new CharacterBuff(buffs[index]);
        }
    }
}
