using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace homelleon.Battler.Implementation.Buffs
{
	public class CharacterBuff
	{
		public BuffType Type { get; private set; }
		public List<Modificator> Modificators { get; private set; }
		public int MaxDuration { get; private set; }
		public ReactiveProperty<int> DurationLeft { get; private set; } = new ReactiveProperty<int>();
		public CharacterBuff(BuffRawData rawData)
		{
			Type = rawData.Type;
			Modificators = rawData.Modificators.ToList();
			MaxDuration = rawData.MaxDuration;
			DurationLeft.Value = MaxDuration;
		}

		public void Consume()
		{
			DurationLeft.Value -= 1;
			if (DurationLeft.Value <= 0)
				Modificators.ForEach(mod => mod.OnRemove?.Invoke());
		}
	}
}
