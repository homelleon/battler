using System;

namespace homelleon.Battler.Implementation.Buffs
{
	[Serializable]
	public class BuffRawData
	{
		public BuffType Type;
		public Modificator[] Modificators;
		public int MaxDuration;
	}
}
