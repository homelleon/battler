using System;
using UnityEngine;

namespace homelleon.Battler.Implementation.Buffs
{
	[Serializable]
	public class BuffSprite
	{
		public BuffType Type;
		public Sprite Sprite;
	}
}
