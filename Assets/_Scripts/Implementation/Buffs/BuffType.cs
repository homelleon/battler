namespace homelleon.Battler.Implementation.Buffs
{
	public enum BuffType
	{
		DoubleDamage,
		ArmorSelf,
		ArmorDestructor,
		VampirismSelf,
		VampirismDecrease
	}
}
