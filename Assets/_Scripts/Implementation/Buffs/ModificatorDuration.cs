namespace homelleon.Battler.Implementation.Buffs
{
	public enum ModificatorDuration
	{
		Permanent,
		Temporal
	}
}
