using homelleon.Battler.Implementation.View.UI.Buttons;
using UnityEngine;

namespace homelleon.Battler.Implementation.View.UI
{
	public class CharacterControlUI : MonoBehaviour
	{
		public AttackButtonUI AttackButton { get; private set; }
		public BuffButtonUI BuffButton { get; private set; }
		private void Awake()
		{
			AttackButton = GetComponentInChildren<AttackButtonUI>(true);
			BuffButton = GetComponentInChildren<BuffButtonUI>(true);
		}
	}
}
