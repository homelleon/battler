using homelleon.Battler.Configs;
using homelleon.Battler.Implementation.Buffs;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace homelleon.Battler.Implementation.View.UI
{
	public class BuffsContainerUI : MonoBehaviour
	{
		private Dictionary<BuffType, BuffUI> _buffs = new Dictionary<BuffType, BuffUI>();
		private Queue<BuffUI> _freeBuffs = new Queue<BuffUI>();
		private GameObject _prefab;
		private Dictionary<BuffType, Sprite> _sprites;

		[Inject]
		private void Initialize(BuffsConfig config)
		{
			_prefab = config.Prefab;
			_sprites = config.Sprites.ToDictionary(pair => pair.Type, pair => pair.Sprite);
		}

		public void Add(CharacterBuff data)
		{
			var buff = _freeBuffs.Count > 0 ? 
				_freeBuffs.Dequeue() : 
				CreateNewBuffUI();

			_buffs.Add(data.Type, buff);

			buff.gameObject.SetActive(true);
			buff.Subscribe(data, () => Return(data.Type));
			buff.SetSprite(_sprites[data.Type]);
		}

		private BuffUI CreateNewBuffUI() => Instantiate(_prefab, transform).GetComponent<BuffUI>();

		private void Return(BuffType type)
		{
			if (_buffs.TryGetValue(type, out var buff))
			{
				_buffs.Remove(type);
				_freeBuffs.Enqueue(buff);
				buff.Unsubscribe();
				buff.gameObject.SetActive(false);
			}
		}
	}
}
