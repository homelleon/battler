using homelleon.Battler.Implementation.View.UI.Buttons;
using UnityEngine;

namespace homelleon.Battler.Implementation.View.UI
{
	public class CanvasUI : MonoBehaviour
	{
		public CharacterUI Left { get; private set; }
		public CharacterUI Right { get; private set; }
		public TurnViewUI Turns { get; private set; }
		public RestartUI RestartUI { get; private set; }

		private void Awake()
		{
			var uis = GetComponentsInChildren<CharacterUI>(true);
			Left = uis[0];
			Right = uis[1];
			Turns = GetComponentInChildren<TurnViewUI>(true);
			RestartUI = GetComponentInChildren<RestartUI>(true);
		}
	}
}
