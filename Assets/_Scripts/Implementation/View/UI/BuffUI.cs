using homelleon.Battler.Configs;
using homelleon.Battler.Implementation.Buffs;
using System;
using System.Linq;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace homelleon.Battler.Implementation.View.UI
{
	public class BuffUI : MonoBehaviour
	{
		private TextMeshProUGUI _durationText;
		private Image _image;
		private IDisposable _onDurationListener;
		private Action _onExpire;
		private BuffsConfig _buffConfig;

		private void Awake()
		{
			_durationText = GetComponentInChildren<TextMeshProUGUI>(true);
			_image = GetComponentInChildren<Image>(true);
		}

		[Inject]
		private void Initialize(BuffsConfig buffConfig)
		{
			_buffConfig = buffConfig;
		}

		public void Subscribe(CharacterBuff buff, Action onExpire)
		{
			_onDurationListener = buff.DurationLeft.Subscribe(OnDuration);
			_onExpire = onExpire;
		}

		public void SetSprite(Sprite sprite) => _image.sprite = sprite;

		private void OnDuration(int value)
		{
			if (value <= 0)
				_onExpire?.Invoke();
			else
				_durationText.text = value.ToString();
		}

		public void Unsubscribe()
		{
			_onDurationListener?.Dispose();
		}

		private void OnDestroy() => Unsubscribe();
	}
}
