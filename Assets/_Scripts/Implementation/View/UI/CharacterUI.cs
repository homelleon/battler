using UnityEngine;

namespace homelleon.Battler.Implementation.View.UI
{
	public class CharacterUI : MonoBehaviour
	{
		public CharacterControlUI Control { get; private set; }
		public BuffsContainerUI Buffs { get; private set; }
		private void Awake ()
		{
			Control = GetComponentInChildren<CharacterControlUI>();
			Buffs = GetComponentInChildren<BuffsContainerUI>();
		}
	}
}
