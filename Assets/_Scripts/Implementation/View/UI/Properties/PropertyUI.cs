using TMPro;
using UnityEngine;

namespace homelleon.Battler.Implementation.View.UI.Properties
{
	public class PropertyUI : PropertyBaseUI
	{
		[SerializeField] private TextMeshProUGUI _labelContainer;
		[SerializeField] private TextMeshProUGUI _valueContainer;
		public void SetValue(float value) => _valueContainer.text = value.ToString();
		public void SetLabel(string label) => _labelContainer.text = label;

	}
}
