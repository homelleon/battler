using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace homelleon.Battler.Implementation.View.UI.Properties
{
	public class PropertyBarUI : PropertyBaseUI
	{
		[SerializeField] private Scrollbar _scroll;
		[SerializeField] private Image _image;
		[SerializeField] private TextMeshProUGUI _textContainer;

		public void SetColor(Color color) => _image.color = color;
		public void SetValue(float value) => _scroll.size = value;
		public void SetLabel(string label) => _textContainer.text = label;
	}
}
