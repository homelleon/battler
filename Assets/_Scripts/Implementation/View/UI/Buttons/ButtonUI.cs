using UnityEngine;
using UnityEngine.UI;

namespace homelleon.Battler.Implementation.View.UI.Buttons
{
	[RequireComponent(typeof(Button))]
	public class ButtonUI : MonoBehaviour
	{
		public Button Button { get; private set; }
		private void Awake()
		{
			Button = GetComponent<Button>();
		}
	}
}
