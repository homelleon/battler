using UnityEngine.UI;

namespace homelleon.Battler.Implementation.View.UI.Buttons
{
	public class RestartUI : ButtonUI
	{
		public void Hide()
		{
			transform.GetChild(0).gameObject.SetActive(false);
			GetComponent<Image>().enabled = false;
		}
		public void Show()
		{
			transform.GetChild(0).gameObject.SetActive(true);
			GetComponent<Image>().enabled = true;
		}
	}
}
