using TMPro;
using UnityEngine;

namespace homelleon.Battler.Implementation.View.UI
{
	public class TurnViewUI : MonoBehaviour
	{
		private TextMeshProUGUI _textPanel;
		private void Awake()
		{
			_textPanel = GetComponentInChildren<TextMeshProUGUI>(true);
		}

		public string Text
		{ 
			get => _textPanel.text; 
			set => _textPanel.text = value; 
		}
	}
}
