using homelleon.AudioJect;
using homelleon.Battler.Implementation.FSM.Turns;
using System;
using UnityEngine;
using Zenject;

namespace homelleon.Battler.Implementation.View
{
	public class CharacterMono : MonoBehaviour
	{
		[SerializeField] private ParticleSystem _bloodParticles;
		[SerializeField] private ParticleSystem _buffParticles;
		private IAudioManager _manager;
		private IFSMTurns _turns;
		[field: SerializeField] public Animator Animator { get; private set; }
		public Action OnNext;

		private void OnValidate()
		{
			Animator ??= GetComponent<Animator>();
		}

		[Inject]
		private void Initialize(IAudioManager manager, IFSMTurns turns)
		{
			_manager = manager;
			_turns = turns;
		}

		public void PlaySound(string track) => _manager.Play(track);
		public void PlayBloodVFX() => _bloodParticles.Play();
		public void PlayBuffVFX() => _buffParticles.Play();
		public void EndDeath() => _turns.Switch<GameOverTurnState>();
		public void NextAnimation() => OnNext?.Invoke();
	}
}
