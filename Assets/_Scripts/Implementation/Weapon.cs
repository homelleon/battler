using homelleon.Battler.Implementation.PropertyData;

namespace homelleon.Battler.Implementation
{
	public class Weapon
	{
		public CharacterProperty Damage { get; private set; }
		public Weapon(CharacterProperty damage)
		{
			Damage = damage;
		}
	}
}
