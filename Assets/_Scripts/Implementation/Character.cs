using homelleon.Battler.Implementation.Buffs;
using homelleon.Battler.Implementation.PropertyData;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using homelleon.Battler.Implementation.View;
using UniRx.Triggers;
using homelleon.Battler.Implementation.FSM.CharacterAnimations;
using UnityEngine.UI;
using UnityEngine;

namespace homelleon.Battler.Implementation
{
	public class Character : IDisposable
	{
		public Weapon Weapon { get; private set; }
		private Dictionary<PropertyType, CharacterProperty> _propertiesPerType;

		private Dictionary<PropertyType, float> _startValues = new Dictionary<PropertyType, float>();
		private Dictionary<CharacterBuff, IDisposable> _buffList = new Dictionary<CharacterBuff, IDisposable>();
		private CharacterAnimator _animator;
		private IDisposable _onHpChanged;
		private IDisposable _onDestroy;
		public Character(CharacterMono mono, List<CharacterProperty> properties, Weapon weapon) 
		{
			Weapon = weapon;
			_propertiesPerType = properties
				.Union(new List<CharacterProperty> { weapon.Damage })
				.ToDictionary(property => property.Type, property => property);
			_startValues = properties
				.ToDictionary(property => property.Type, property => property.ModifiedContainer.Value);
			var hp = _propertiesPerType[PropertyType.HP];
			_onHpChanged = hp.ModifiedContainer
				.Skip(1)
				.Subscribe(OnHpChanged);

			_onDestroy = mono
				.OnDestroyAsObservable()
				.Subscribe(unit => Dispose());

			_animator = new CharacterAnimator(mono, hp);
		}

		private void OnHpChanged(float value)
		{
			if (value <= 0)
				Kill();
		}

		public void Attack() => _animator.Switch<CharacterFightState>();

		public void Damage() => _animator.Switch<CharacterDamagedState>();

		public void Kill() => _animator.Switch<CharacterDeathState>();

		public void AddBuff(CharacterBuff buff)
		{
			var listener = buff.DurationLeft.Subscribe(value => OnDurationLeft(value, buff));
			_buffList.Add(buff, listener);
			_animator.Switch<CharacterBuffState>();
			var onceBuffs = buff.Modificators.Where(mod => mod.ExecuteType == BuffExecuteType.Once);

			foreach (var mod in onceBuffs)
			{
				Debug.Log(mod.TargetProperty);
				GetProperty(mod.TargetProperty).Modify(mod);
			}
		}

		public void Revive()
		{
			ClearBuffs();
			foreach (var item in _propertiesPerType)
			{
				var type = item.Key;
				item.Value.ModifiedContainer.Value = _startValues[type];
			}
			_animator.Switch<CharacterIdleState>();
		}

		private void ClearBuffs()
		{
			foreach (var buff in _buffList.Keys.ToList())
				buff.DurationLeft.Value = 0;

			_buffList.Clear();
		}

		private void OnDurationLeft(int value, CharacterBuff buff)
		{
			if (value <= 0)
			{
				_buffList[buff]?.Dispose();
				_buffList.Remove(buff);
			}
		}

		public List<CharacterBuff> GetBuffList() => _buffList.Keys.ToList();

		public CharacterProperty GetProperty(PropertyType type) => _propertiesPerType[type];

		public void Dispose()
		{
			ClearBuffs();
			_onHpChanged?.Dispose();
			_onDestroy?.Dispose();
		}
	}
}
