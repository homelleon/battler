using homelleon.Battler.Implementation.Buffs;
using homelleon.Battler.Implementation.PropertyData;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace homelleon.Battler.Implementation
{
	public class DamageChain
	{
		private Character _source;
		private Character _target;
		public DamageChain(Character source, Character target)
		{
			_source = source;
			_target = target;
		}

		public void Invoke()
		{
			var sourceMods = _source.GetBuffList()
				.SelectMany(buff => buff.Modificators)
				.Where(modificator => modificator.ExecuteType == Buffs.BuffExecuteType.OnAttack);

			ApplyMods(sourceMods.Where(mod => mod.Target == ModificatorTarget.Self), _source);
			ApplyMods(sourceMods.Where(mod => mod.Target == ModificatorTarget.Enemy), _target);

			var damage = _source.GetProperty(PropertyType.Damage).ModifiedContainer.Value;
			var vampirism = _source.GetProperty(PropertyType.Vampirism).ModifiedContainer.Value;
			var sourceHP = _source.GetProperty(PropertyType.HP);


			var armor = _target.GetProperty(PropertyType.Armor).ModifiedContainer.Value;
			var targetHP = _target.GetProperty(PropertyType.HP);

			if (armor > 0)
				damage /= 0.1f * armor;

			targetHP.ModifiedContainer.Value = Mathf.Max(targetHP.ModifiedContainer.Value - damage, targetHP.Min);

			if (vampirism > 0)
			{
				damage *= 1 / vampirism;
				sourceHP.ModifiedContainer.Value = Mathf.Min(sourceHP.ModifiedContainer.Value + damage, sourceHP.Max);
			}

			_source.Attack();
			_target.Damage();
		}

		private void ApplyMods(IEnumerable<Modificator> allMods, Character character)
		{
			foreach (var mod in allMods)
			{
				var property = character.GetProperty(mod.TargetProperty);
				property.Modify(mod);
			}
		}
	}
}
