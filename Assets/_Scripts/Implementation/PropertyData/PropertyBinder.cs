using homelleon.Battler.Implementation.View.UI.Properties;
using homelleon.Battler.Implementation.ViewModel;
using System.Collections.Generic;

namespace homelleon.Battler.Implementation.PropertyData
{
	public class PropertyBinder : IPropertyBinder
	{
		private PropertyContract _defaultContract = new PropertyContract
		{
			ViewModel = typeof(PropertyBarViewModel),
			View = typeof(PropertyBarUI),
		};

		private Dictionary<PropertyType, PropertyContract> _contracts = new Dictionary<PropertyType, PropertyContract>()
		{ 
			[PropertyType.Damage] = new PropertyContract{ ViewModel = typeof(PropertyViewModel), View = typeof(PropertyUI)},
		};


		public PropertyContract Get(PropertyType type) =>
			_contracts.ContainsKey(type) ?
				_contracts[type] :
				_defaultContract;
	}
}
