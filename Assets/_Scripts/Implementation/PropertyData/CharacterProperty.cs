using homelleon.Battler.Implementation.Buffs;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace homelleon.Battler.Implementation.PropertyData
{
	public class CharacterProperty : ICloneable, IDisposable
	{
		public PropertyType Type { get; private set; }
		public ReactiveProperty<float> UnmodifiedContainer { get; private set; } = new ReactiveProperty<float>();
		public ReactiveProperty<float> ModifiedContainer { get; private set; } = new ReactiveProperty<float>();
		public float Min { get; private set; }
		public float Max { get; private set; }
		private List<Modificator> _modificators = new List<Modificator>();
		private IDisposable _onValueChangedListener;

		public CharacterProperty(PropertyType type, float min, float max)
		{
			Type = type;
			Min = min;
			Max = max;
			_onValueChangedListener = UnmodifiedContainer.Subscribe(OnValueChanged);
		}

		private void OnValueChanged(float value) => ApplyModificators();

		public void Modify(Modificator modificator)
		{
			_modificators.Add(modificator);
			modificator.OnRemove = () => Unmodify(modificator);
			ApplyModificators();
		}

		public bool Unmodify(Modificator modificator)
		{
			if (_modificators.Remove(modificator))
			{
				ApplyModificators();
				return true;
			}

			return false;
		}

		private void ApplyModificators() 
		{
			var value = UnmodifiedContainer.Value;
			foreach (var mod in _modificators)
				mod.Apply(ref value);

			Debug.Log("Prop: " + Type);
			Debug.Log("mods: " + string.Join(", ", _modificators.Select(m => m.Type)));
			Debug.Log("StartValue: " + UnmodifiedContainer.Value);

			ModifiedContainer.Value = Mathf.Clamp(value, Min, Max);
			Debug.Log("Value: " + ModifiedContainer.Value);
		}

		public object Clone()
		{
			var property = new CharacterProperty(Type, Min, Max);
			property.UnmodifiedContainer.Value = UnmodifiedContainer.Value;
			property.ModifiedContainer.Value = UnmodifiedContainer.Value;
			return property;
		}

		public void Dispose()
		{
			_onValueChangedListener?.Dispose();
		}
	}
}
