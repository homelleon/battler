using System;
using UnityEngine;

namespace homelleon.Battler.Implementation.PropertyData
{
	[Serializable]
	public class PropertyRawData
	{
		public PropertyType Type;
		public float Value;
		public float Min;
		public float Max;
		public GameObject Prefab;
	}
}
