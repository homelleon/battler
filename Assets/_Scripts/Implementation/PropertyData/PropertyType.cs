using System;

namespace homelleon.Battler.Implementation.PropertyData
{
	[Serializable]
	public enum PropertyType
	{
		HP,
		Armor,
		Vampirism,
		Damage
	}
}
