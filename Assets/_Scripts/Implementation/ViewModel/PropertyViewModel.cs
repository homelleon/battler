using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View.UI.Properties;
using System;
using UniRx;

namespace homelleon.Battler.Implementation.ViewModel
{
	public class PropertyViewModel : IDisposable
	{
		private PropertyUI _view;
		private IDisposable _onValueListener;
		public PropertyViewModel(PropertyUI view, CharacterProperty data)
		{
			_view = view;
			_view.SetLabel(data.Type.ToString());
			data.ModifiedContainer.Subscribe(OnValue);
		}
		private void OnValue(float value) => _view.SetValue(value);
		public void Dispose() => _onValueListener?.Dispose();
	}
}
