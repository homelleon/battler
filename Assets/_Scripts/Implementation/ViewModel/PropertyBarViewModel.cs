using System;
using UnityEngine;
using UniRx;
using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View.UI.Properties;

namespace homelleon.Battler.Implementation.ViewModel
{
	public class PropertyBarViewModel : IDisposable
	{
		private PropertyBarUI _view;
		private float _unitValue;
		private IDisposable _onValueListener;

		public PropertyBarViewModel(PropertyBarUI view, CharacterProperty data)
		{
			_view = view;
			_onValueListener = data.ModifiedContainer.Skip(1).Subscribe(OnValue);
			_unitValue = 1 / Mathf.Max(data.Max - data.Min, Mathf.Epsilon);
			view.SetValue(_unitValue * data.ModifiedContainer.Value);
			view.SetLabel(data.Type.ToString());
			view.SetColor(GetColor(data.Type));

		}

		private Color GetColor(PropertyType type)
		{
			switch (type)
			{
				case PropertyType.HP: return Color.red;
				case PropertyType.Armor: return Color.yellow;
				case PropertyType.Vampirism: return Color.blue;
				default: return Color.white;
			}
		}

		private void OnValue(float value) => _view.SetValue(_unitValue * value);

		public void Dispose() => _onValueListener?.Dispose();
	}
}
