using homelleon.Battler.Configs;
using homelleon.Battler.Implementation.FSM.Turns;
using homelleon.Battler.Implementation.PropertyData;
using homelleon.Battler.Implementation.View.UI;
using homelleon.Battler.Implementation.View;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;
using homelleon.AudioJect;

namespace homelleon.Battler.Implementation
{
	public class Game : MonoBehaviour
	{
		[SerializeField] private CharacterMono _leftCharacterMono;
		[SerializeField] private CharacterMono _rightCharacterMono;
		[SerializeField] private Transform _leftProperties;
		[SerializeField] private Transform _rightProperties;
		[SerializeField] private CanvasUI _charactersUI;

		private IFSMTurns _turns;
		private IFSMPhases _phases;
		private IBattleRoundCounter _counter;
		private PropertyConfigs _propertyConfigs;
		private IBuffGenerator _buffGenerator;
		private IPropertyBinder _propertyBinder;
		private IAudioManager _audio;
		private readonly List<IDisposable> _disposables = new List<IDisposable>();

		private const string MUSIC_NAME = "ThemeSong";

		[Inject]
		private void Initialize(IFSMTurns turns, IFSMPhases phases, IBattleRoundCounter counter, 
			PropertyConfigs propertyConfigs, IBuffGenerator buffGenerator,
			IPropertyBinder propertyBinder, IAudioManager audio)
		{
			_turns = turns;
			_phases = phases;
			_counter = counter;
			_propertyConfigs = propertyConfigs;
			_buffGenerator = buffGenerator;
			_propertyBinder = propertyBinder;
			_audio = audio;
		}

		private void Start()
		{
			var leftCharProperties = _propertyConfigs.Properties
				.Select(raw =>
				{
					var property = new CharacterProperty(raw.Type, raw.Min, raw.Max);
					property.UnmodifiedContainer.Value = raw.Value;
					return property;
				})
				.ToList();

			var rightCharProperties = leftCharProperties
				.Select(property => property.Clone())
				.Cast<CharacterProperty>().ToList();

			InitUI(new Dictionary<Transform, List<CharacterProperty>>
			{
				[_leftProperties] = leftCharProperties,
				[_rightProperties] = rightCharProperties
			});

			var leftWeapon = new Weapon(leftCharProperties.First(property => property.Type == PropertyType.Damage));
			var rightWeapon = new Weapon(rightCharProperties.First(property => property.Type == PropertyType.Damage));

			var leftCharacter = new Character(_leftCharacterMono, leftCharProperties, leftWeapon);
			var rightCharacter = new Character(_rightCharacterMono, rightCharProperties, rightWeapon);

			var leftAttack = new DamageChain(leftCharacter, rightCharacter);
			var rightAttack = new DamageChain(rightCharacter, leftCharacter);

			var leftButtons = _charactersUI.Left.Control;
			var rightButtons = _charactersUI.Right.Control;

			leftButtons.AttackButton.Button.onClick.AddListener(() => leftAttack.Invoke());
			rightButtons.AttackButton.Button.onClick.AddListener(() => rightAttack.Invoke());

			_counter.Text.Subscribe(text => _charactersUI.Turns.Text = text).AddTo(this);

			var hitPoints = new List<CharacterProperty>()
			{
				leftCharacter.GetProperty(PropertyType.HP),
				rightCharacter.GetProperty(PropertyType.HP)
			};

			_turns.Add(new LeftPlayerTurnState(_turns, _charactersUI.Left, _counter, leftCharacter, hitPoints, _buffGenerator));
			_turns.Add(new RightPlayerTurnState(_turns, _charactersUI.Right, _counter, rightCharacter, hitPoints, _buffGenerator));
			_turns.Add(new GameOverTurnState(_turns, _charactersUI, new List<Character> { leftCharacter, rightCharacter }, _counter));
			_turns.Switch<LeftPlayerTurnState>();
			_audio.Play(MUSIC_NAME, true);
		}

		private void InitUI(Dictionary<Transform, List<CharacterProperty>> map)
		{
			var prefabs = _propertyConfigs.Properties.ToDictionary(property => property.Type, property => property.Prefab);

			foreach (var pair in map)
			{
				var parent = pair.Key;

				foreach (var property in pair.Value)
				{
					var viewGO = Instantiate(prefabs[property.Type], parent);
					var contract = _propertyBinder.Get(property.Type);
					var view = viewGO.GetComponent(contract.View);
					Activator.CreateInstance(contract.ViewModel, view, property);
				}
			}
		}

		private void OnDestroy()
		{
			_disposables.ForEach(disposable => disposable.Dispose());
			_disposables.Clear();
			_audio.Stop();
		}
	}
}
