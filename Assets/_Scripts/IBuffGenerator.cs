using homelleon.Battler.Implementation.Buffs;
using System.Collections.Generic;

namespace homelleon.Battler
{
	public interface IBuffGenerator
	{
		CharacterBuff Generate(List<BuffType> excludeBuffs = null);
	}
}
