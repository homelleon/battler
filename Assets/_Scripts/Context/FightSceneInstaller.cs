using homelleon.AudioJect.Config;
using homelleon.AudioJect.Implementation;
using homelleon.Battler.Configs;
using homelleon.Battler.Implementation;
using homelleon.Battler.Implementation.Buffs;
using homelleon.Battler.Implementation.FSM.Phases;
using homelleon.Battler.Implementation.FSM.Turns;
using homelleon.Battler.Implementation.PropertyData;
using UnityEngine;
using Zenject;

namespace homelleon.Battler.Context
{
    [CreateAssetMenu(fileName = "FightSceneInstaller", menuName = "Installers/FightSceneInstaller")]
    public class FightSceneInstaller : ScriptableObjectInstaller<FightSceneInstaller>
    {
        [SerializeField] private PropertyConfigs _properties;
        [SerializeField] private BuffsConfig _buffs;
        [SerializeField] private AudioConfig _audioConfig;
        [SerializeField] private AudioTracksConfig _trackConfig;
        public override void InstallBindings()
        {
            Container.BindInstances(_properties, _buffs, _audioConfig, _trackConfig);
            Container.BindFactory<Transform, AudioSource, AudioSourceFactoryPlaceholder>().FromFactory<AudioSourceFactory>();
            Container.BindInterfacesAndSelfTo<AudioSourcePool>().FromNewComponentOnNewGameObject().AsSingle();
            Container.BindInterfacesAndSelfTo<AudioManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<PropertyBinder>().AsSingle();
            Container.BindInterfacesAndSelfTo<BuffGenerator>().AsSingle();
            Container.BindInterfacesAndSelfTo<BattleRoundCounter>().AsSingle();
            Container.BindInterfacesAndSelfTo<PhasesStates>().AsSingle();
            Container.BindInterfacesAndSelfTo<FSMTurns>().AsSingle();
        }
    }
}