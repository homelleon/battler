using UniRx;

namespace homelleon.Battler
{
	public interface IBattleRoundCounter
	{
		ReactiveProperty<string> Text { get; }
		void Increment();
		void Reset();
	}
}
