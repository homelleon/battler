using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace homelleon.Battler.Configs
{
	[CreateAssetMenu(fileName = "VikingConfigs", menuName = "Settings/VikingConfigs")]
	public class VikingConfigs : ScriptableObject
	{
		[SerializeField] private Sprite[] _sprites;
		public Sprite[] Sprites => _sprites;

        private const string spritesPath = "Images/Viking";
        private const string atlasName = "Viking.spriteatlasv2";

        private void OnValidate()
        {
            //var bundle = AssetBundle
            //    .LoadFromFile(Application.dataPath + "/" + spritesPath);

            //var sprites = bundle.GetAllAssetNames()
            //    .Where(name => name.EndsWith(".png"))
            //    .Select(name => bundle.LoadAsset<Sprite>(name));
            
            //_sprites = sprites.ToArray(); 

            //foreach (var item in bandles)
            //    item.Unload(true); 
        }
    }

}
