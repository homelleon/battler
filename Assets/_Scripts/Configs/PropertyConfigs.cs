using homelleon.Battler.Implementation.PropertyData;
using System.Collections.Generic;
using UnityEngine;

namespace homelleon.Battler.Configs
{
    [CreateAssetMenu(fileName = "PropertyConfigs", menuName = "Settings/PropertyConfigs")]
	public class PropertyConfigs : ScriptableObject
	{
		[field: SerializeField] public List<PropertyRawData> Properties { get; private set; }
    }
}
