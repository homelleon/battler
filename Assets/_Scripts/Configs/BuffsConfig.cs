using homelleon.Battler.Implementation.Buffs;
using UnityEngine;

namespace homelleon.Battler.Configs
{
	[CreateAssetMenu(fileName = "BuffsConfig", menuName = "Settings/BuffsConfig")]
	public class BuffsConfig : ScriptableObject
	{
		[field: SerializeField] public BuffRawData[] Array { get; private set; }
		[field: SerializeField] public GameObject Prefab { get; private set; }
		[field: SerializeField] public BuffSprite[] Sprites { get; private set; }
    }
}
