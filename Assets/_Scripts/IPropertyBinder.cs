using homelleon.Battler.Implementation.PropertyData;

namespace homelleon.Battler
{
	public interface IPropertyBinder
	{
		PropertyContract Get(PropertyType type);
	}
}
