using homelleon.Battler.Implementation.FSM;
using System;

namespace homelleon.Battler
{
	public interface IFinitStateMachine<T> where T: BaseState
	{
		void Switch<V>() where V : T;
		void Switch(Type type);
		void Update();
		void Add(T state);
	}
}
